import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FizzBuzz {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
    	int n = Integer.parseInt(line);
    	boolean f, b, fb = true;
    	for (int i=1; i<=n;i++){
    		if(i%3==0 &&i%5==0){
    			System.out.println("FizzBuzz");
    		} else if(i%3==0){
    			System.out.println("Fizz");
    		} else if(i%5==0){
    			System.out.println("Buzz");
    		} else{
    			System.out.println(i);
    		}
    	}
	}

}
