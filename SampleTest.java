import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class SampleTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(missingTermInAP());
		System.out.println(getNumberOfPrimes(100));
		
	}
	/*
	 * Complete the function below.
	 */

	static int getNumberOfPrimes(int N) {
		int numberOfPrimes =0;
		for(int i=2; i<N; i++){
			if(isPrime(i)){
				numberOfPrimes++;
			}
		}
		return numberOfPrimes;


	}
	static boolean isPrime(int N){
		for(int i=2; i<N/2+1; i++){
				if(N%i==0){
					return false;
				}
		}
		return true;
	}

	
	
	
	
	///////////////////////////////////////////////////////////////////////////
	///////////////////////          2             ////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	private static class Node {
	       Node left, right;
	       int data;
	       Node(int newData) {
	           left = right = null;
	           data = newData;
	       }
	}

	private static Node insert(Node node, int data) {
	       if (node==null){            
	           node = new Node(data);
	       }
	       else {
	           if (data < node.data) {
	               node.left = insert(node.left, data);
	           }
	           else {
	               node.right = insert(node.right, data);
	           }
	       }
	       return(node);
	   }

	private static Node addRandomElement(Node node, int data, int index){
	       if (node==null){            
	           node = new Node(data);
	       }
	       else {
	            if (index <= 2) {
	               node.left = addRandomElement(node.left, data,index);
	           }
	           else {
	               node.right = addRandomElement(node.right, data,index);
	           }
	  
	       }
	       return(node);
	   }
	
	/* Write your custom functions here */
	//static int diameterOfTree(Node root) {
	/* For your reference
	   class Node {
	       Node left, right;
	       int data;
	               Node(int newData) {
	           left = right = null;
	           data = newData;
	       }
	   }
	*/     
	//}
	
	/*public static void main(String[] args) throws IOException {
		  Scanner in = new Scanner(System.in);
		  final String fileName = System.getenv("OUTPUT_PATH");
		  BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
		  
		   Node _root;
		int root_i=0, root_cnt = 0, root_num = 0;
		   _root = null;
		   int isBst = in.nextInt();
		       root_cnt = in.nextInt();
		for(root_i = 0; root_i < root_cnt; root_i++){
		      root_num = in.nextInt();
		      if( isBst == 0 ){                         _root = addRandomElement(_root,root_num,root_i);
		        } else {
		    _root = insert(_root, root_num);
		          }                
		     }

		     bw.write(String.valueOf(diameterOfTree(_root)));
		     bw.newLine();
			 bw.close();
		   return; 
		 }
		 
	}*/
	
	///////////////////////////////////////////////////////////////////////////
	///////////////////////          1             ////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	static int missingTermInAP(){
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
    	String list = scanner.nextLine();
		list = scanner.nextLine();
		String[] ints = list.split(" ");
		int[] nums = new int[ints.length];
		for(int i =0; i <ints.length;i++){
			nums[i] =Integer.parseInt(ints[i]);
		}
		/*int diff1, diff2;
		diff1 = nums[1] -nums[0];
		diff2= nums[2] - nums[1];
		int i=3;
		while(diff1!=diff2){
			diff1= diff2;
			diff2=nums[i]- nums[i-1];
		}
		*/
		int apSum = (n+1)*(nums[0]+nums[n-1])/2;
		System.out.println("APSUM = "+ apSum);
		int givenSum =0;
		for(int i =0;i<nums.length;i++){
			givenSum+=nums[i];
		}
		System.out.println("givenSum = "+ givenSum);
		scanner.close();
		return apSum-givenSum;
	}


}
