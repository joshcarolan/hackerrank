import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class CoinChangeProblem {
	public static int Count(int Sum,int[] coins,int index,int curSum)
	{
	    int count=0;

	    if (index>=coins.length)
	        return 0;

	    int sumNow=curSum+coins[index];
	    if (sumNow>Sum)
	        return 0;
	    if (sumNow==Sum)
	        return 1;

	    for (int i= index+1;i<coins.length;i++)
	        count+=Count(Sum,coins,i,sumNow);

	    return count;       
	}
    public static void main(String args[])  throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        String [] ints = line.split(" ");
        
    	int n = Integer.parseInt(ints[0]);
        int m = Integer.parseInt(ints[1]);
        line = br.readLine();
        ints = line.split(" ");
        int[] coins = new int[m];
        for(int i=0; i<m; i++){
            coins[i] = Integer.parseInt(ints[i]);
        }
        int count = 0;
        for(int i = 0; i<m; i++){
        	count+=Count(n, coins, i, 0);
        }
        Count();
    }
}
