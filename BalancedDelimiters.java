import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
public class BalancedDelimiters {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		String string = scanner.nextLine();
		int bracket= 0;
		int brace =0;
		int square =0;
		for(int i = 0; i<string.length();i++){
			if(string.charAt(i) == '['){
				square ++;				
			} else if(string.charAt(i) == '('){
				bracket ++;				
			} else if(string.charAt(i)=='{'){
				brace++;
			} else if(string.charAt(i)==']'){
				square--;
			}else if(string.charAt(i) == ')'){
				bracket--;				
			} else if(string.charAt(i)=='}'){
				brace--;
			}
		}
		
		int total = bracket+brace+square;
		if (total==0){
			System.out.println("True");
		}
		else{
			System.out.println("False");
		}
		scanner.close();
	}

}
