import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class MthToLast {
    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
    	int m = Integer.parseInt(line);
    	line = br.readLine();
		String[] ints = line.split(" ");
		int len = ints.length;
		if(len-m>=0){
			System.out.println(ints[len-m]);
		}
		else{
			System.out.println("NIL");
		}
    }
}
