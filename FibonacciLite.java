import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FibonacciLite {
	
	 public static void main(String[] args) {
	        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
		 	Scanner scanner = new Scanner(System.in);
		 	int n = scanner.nextInt();
		 	System.out.println(fibonacci(n));
		 	
	 }
	 
	 public static int fibonacci(int n){
		 if(n==1 || n==0){
			 return n;
		 }
		 else{
			 return fibonacci(n-1) + fibonacci(n-2);
		 }
	 }
	 
}
