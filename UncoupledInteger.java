import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class UncoupledInteger {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		String list = scanner.nextLine();
		list = list.replaceAll("\\s", "");
		String[] ints = list.split(",");
		int total = Integer.parseInt(ints[0]);
		for (int i = 1; i < ints.length; i++) {
			// xor the running total of previous xors from the list with the
			// current int in the list
			total = total ^ Integer.parseInt(ints[i]);
		}
		System.out.println(total);
		scanner.close();

	}

}
